<?php

class Fraction
{
    private $numerator;
    private $denominator;

    function __construct($numerator, $denominator)
    {
        $this->numerator = $numerator;
        $this->denominator = $denominator;
    }

    function getNum(){
        return $this->numerator;
    }

    function getDen(){
        return $this->denominator;
    }

    function gcd($a,$b)
    {
        if ($a==0) return $b;
        else return $this->gcd($b%$a,$a);
    }

    function compact(){
        $ret = clone $this;
        $gcd = $this->gcd($ret->numerator,$ret->denominator);
        $ret->numerator /= $gcd;
        $ret->denominator /= $gcd;

        return $ret;
    }

    function add($frac)
    {
        $ret = clone($this);
        if ($frac instanceof Fraction) {
            $ret->numerator = $ret->numerator * $frac->denominator + $frac->numerator * $ret->denominator;
            $ret->denominator *= $frac->denominator;
        }
        return $ret->compact();
    }

    function sub($frac)
    {
        $ret = clone($this);
        if ($frac instanceof Fraction) {
            $ret->numerator = $ret->numerator * $frac->denominator - $frac->numerator * $ret->denominator;
            $ret->denominator *= $frac->denominator;
        }
        return $ret->compact();
    }

    function mul($frac){
        $ret = clone($this);
        if ($frac instanceof Fraction) {
            $ret->numerator *= $frac->numerator;
            $ret->denominator*=$frac->denominator;
            return $ret->compact();
        }
    }

    function div($frac){
        $ret = clone($this);
        if ($frac instanceof Fraction) {
            $ret->numerator*=$frac->denominator;
            $ret->denominator*=$frac->numerator;
            return $ret->compact();
        }
    }
}

$frac1 = new Fraction(3,5);
$frac2 = new Fraction(7,9);
$frac3 = new Fraction(2,9);

$frac = $frac1->mul($frac2)->sub($frac1->mul($frac3));
echo $frac->getNum() . "/" . $frac->getDen();

