<?php

class InvoiceItem
{
    private $id;
    private $desc;
    private $qty;
    private $unitPrice;

    function __construct($id, $desc, $qty, $unitPrice)
    {
        $this->id = $id;
        $this->desc = $desc;
        $this->qty = $qty;
        $this->unitPrice = $unitPrice;
    }

    function getId()
    {
        return $this->id;
    }
    function getDesc(){
        return $this->desc;
    }
    function getQty(){
        return $this->qty;
    }

    function getUnitPrice(){
        return $this->unitPrice;
    }

    function setQty($qty){
        $this->qty = $qty;
    }
    function setUnitPrice($unitPrice){
        $this->unitPrice = $unitPrice;
    }

    function getTotal(){
        return $this->unitPrice * $this->qty;
    }

    function toString(){
        return 'InvoiceItem[id= '.$this->id.', desc= '.$this->desc.', qty= '.$this->qty.', unitPrice= '.$this->unitPrice.']';
    }
}