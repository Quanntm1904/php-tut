<?php

class Time
{
    private $hour;
    private $minute;
    private $second;

    function __construct($hour, $minute, $second)
    {
        $this->hour = $hour;
        $this->minute = $minute;
        $this->second = $second;
    }

    function setHour($hour)
    {
        $this->hour = $hour;
    }

    function setMinute($minute)
    {
        $this->minute = $minute;
    }

    function setSecond($second)
    {
        $this->second = $second;
    }

    function getHour()
    {
        return $this->hour;
    }

    function getMinute(){
        return $this->minute;
    }

    function getSecond(){
        return $this->second;
    }

    function setTime($hour,$minute,$second){
        $this->hour = $hour;
        $this->minute = $minute;
        $this->second = $second;
    }

    function toString(){
        $h = sprintf("%02d",$this->hour);
        $m = sprintf("%02d",$this->minute);
        $s = sprintf("%02d",$this->second);
        return $h.':'.$m.':'.$s;
    }

    function nextSecond(){
        $this->second += 1;
        return $this;
    }

    function previousSecond(){
        $this->second -= 1;
        return $this;
    }
}