<?php

class Book
{
    private $name;
    private $author;
    private $price;
    private $qty;


    function __construct($name, $author, $price, $qty = 0)
    {
        $this->name = $name;
        $this->author = $author;
        $this->price = $price;
        $this->qty = $qty;
    }

    function getName()
    {
        return $this->name;
    }

    function getAuthor()
    {
        return $this->author;
    }

    function setPrice($price)
    {
        $this->price = $price;
    }

    function getPrice()
    {
        return $this->price;
    }

    function setQty($qty)
    {
        $this->qty = $qty;
    }


    function getQty()
    {
        return $this->qty;
    }

    function toString()
    {
        return 'Book[name= ' . $this->name . ',email= ' . $this->author . ',price= ' . $this->price . 'qty= ' . $this->qty . ']';
    }
}