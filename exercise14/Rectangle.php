<?php

class Rectangle
{
    private $length;
    private $width;

    function __construct($length, $width)
    {
        $this->length = $length;
        $this->width = $width;
    }

    function setLength($length)
    {
        $this->length = $length;
    }

    function setWidth($width)
    {
        $this->width = $width;
    }

    function getLength()
    {
        return $this->length;
    }

    function getWidth()
    {
        return $this->width;
    }

    function getArea()
    {
        return $this->length * $this->width;
    }

    function getPerimeter()
    {
        return 2 * ($this->length + $this->width);
    }

    function toString()
    {
        return 'Rectangle[length=.' . $this->length . ';width = ' . $this->width . ']';
    }
}