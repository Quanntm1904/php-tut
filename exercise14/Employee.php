<?php
class Employee {
    private $id;
    private $firstName;
    private $lastName;
    private $salary;

    function __construct($id,$firstName,$lastName,$salary){
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->salary = $salary;
    }

    function setSalary($salary) {
        $this->salary = $salary;
    }

    function raiseSalary($percentage){
        $this->salary *= (100+$percentage)/100;
    }

    function getSalary() {
        return $this->salary;
    }

    function getAnnualSalary() {
        return $this->salary *12;
    }


    function getId() {
        return $this->id;
    }

    function getFirstName() {
        return $this->firstName;
    }

    function getLastName() {
        return $this->lastName;
    }

    function getName(){
        return 'First Name: ' . $this->firstName . 'Last Name: '.$this->lastName;
    }

    function toString(){
        return "Employee [id=" . $this->id . ", name= ".$this->firstName . " ".$this->lastName .", salary= ".$this->salary."]";
    }
}