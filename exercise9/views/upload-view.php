<?php
require "partials/header.php";
?>

    <div class="upload__container">
        <div class="upload__form">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="file" name="pic[]" multiple>
                <input type="submit" name="submit" value="Upload">
            </form>
        </div>
    </div>

<?php

function clearCookie(){
    $past = time() - 3600;
    foreach ( $_COOKIE as $key => $value )
    {
        setcookie( $key, '', $past);
    }
}

clearCookie();
if (isset($_FILES['pic'])) {
    $imageInfo = $_FILES['pic'];
    function noError($imageInfo){
        foreach ($imageInfo['error'] as $i=> $value){
            if ($value != 0) {
                return false;
            }
        }
        return true;
    }

    if (!noError($imageInfo)) {
        echo "Error!";
    } else {
        for ($i = 0;$i<count($imageInfo['name']);$i++){
            move_uploaded_file($imageInfo['tmp_name'][$i], 'upload/' . $imageInfo['name'][$i]);
            $name = $imageInfo['name'][$i];
            $time = time();
            echo $name."<br>";
            setcookie($name, $time, time() + 3600);
        }

        echo "Uploaded <br>";
        print_r($_COOKIE);

        ?>

        <img src="upload/<?php $name?>" alt="">
<?php
    }
} else {
    echo "Please choose a file";
}
