<?php
session_start();

if (isset($_GET['act']) && $_GET['act'] == 'clear') {
    $_SESSION['stack'] = [];
}

$errors = [];
$person = [
    'mssv' => '',
    'name' => '',
    'birth' => '',
    'phone' => '',
    'adress' => '',
];
$mssv = '';
$name = '';
$birth = '';
$phone = '';
$adress = '';


if (isset($_GET['act']) && $_GET['act'] == 'delete') {

    if (isset($_SESSION['stack'])) {
        $stack = $_SESSION['stack'];
        unset($stack[$_GET['mssv']]);
    }
    $_SESSION['stack'] = $stack;
}

if (isset($_GET['act']) && $_GET['act'] == 'edit') {

    if (isset($_SESSION['stack'])) {
        $stack = $_SESSION['stack'];
        $personalInfo = $stack[$_GET['mssv']];
        $mssv = $personalInfo['mssv'];
        $name = $personalInfo['name'];
        $birth = $personalInfo['birth'];
        $phone = $personalInfo['phone'];
        $adress = $personalInfo['adress'];
    }
    $_SESSION['stack'] = $stack;
}

if (isset($_SESSION['stack'])) {
    $stack = $_SESSION['stack'];
} else $stack = [];

function getError($key)
{
    global $errors;
    return isset($errors[$key]) ? $errors[$key] : '';
}

include "header.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $mssv = $_POST['mssv'] ?? null;
    if (empty($mssv)) {
        $errors['mssv'] = 'MSSV is required';
    } else {
        $person['mssv'] = $mssv;
    }

    $name = $_POST['name'] ?? null;
    if (empty($name)) {
        $errors['name'] = "Name is required";
    } elseif (!preg_match("/.{2,30}/", $name)) {
        $errors['name'] = "2-30 characters";
    } else {
        $person['name'] = $name;
    }

    $birth = $_POST['birth'] ?? null;
    if (empty($birth)) {
        $errors['birth'] = 'Birthday is required';
    } else {
        $person['birth'] = $birth;
    }

    $phone = $_POST['phone'] ?? null;
    if (empty($phone)) {
        $errors['phone'] = 'Phone number is required';
    } elseif (!preg_match('/(?=.*\d){8,12}/', $phone)) {
        $errors['phone'] = 'Phone number must include numbers and have 8-12 characters';
    } else {
        $person['phone'] = $phone;
    }

    $adress = $_POST['adress'] ?? null;
    if (empty($adress)) {
        $errors['adress'] = 'Adress is required';
    } elseif (!preg_match('/.{5,120}/', $adress)) {
        $errors['adress'] = 'Adress have 5-120 characters';
    } else {
        $person['adress'] = $adress;
    }

    if (empty($errors)) {
        unset($stack[$_GET['mssv']]);
        $stack[$person['mssv']] = $person;
        $_SESSION['stack'] = $stack;
        header('Location: info-form.php');
        die;
    }

}
?>
    <h2>Nhập thông tin cá nhân</h2>\
    <form action="" method="POST">

        <span class="error">* <?php echo getError('mssv') ?></span>
        <input type="text" placeholder="MSSV" name="mssv" value="<?php echo $mssv ?>">
        <span class="error">* <?php echo getError('name') ?></span>
        <input type="text" placeholder="Ho va ten" name="name" value="<?php echo $name ?>">
        <span class="error">* <?php echo getError('birth') ?></span>
        <input type="date" placeholder="Nam sinh" name="birth" value="<?php echo $birth ?>">
        <span class="error">* <?php echo getError('phone') ?></span>
        <input type="text" placeholder="SDT" name="phone" value="<?php echo $phone ?>">
        <span class="error">* <?php echo getError('adress') ?></span>
        <input type="text" placeholder="Que quan" name="adress" value="<?php echo $adress ?>">

        <button type="submit" id="submit">Submit</button>

    </form>
    <table id="editable">
        <tr>
            <th>STT</th>
            <th>MSSV</th>
            <th>Ho va ten</th>
            <th>Ngay sinh</th>
            <th>SDT</th>
            <th>Que Quan</th>
            <th>Tuy chon</th>
        </tr>

        <?php
        $i = 1;
        foreach ($stack as $mssv => $personal) {
            echo '<tr>';
            echo '<td>' . $i . '</td>';
            $i++;
            foreach ($personal as $key => $value) {
                echo '<td>' . $value . '</td>';
            }


            ?>
            <td><a href="<?php echo "http://localhost/Test/info-form.php?act=edit&mssv=" . $mssv ?>" class="edit btn">Edit</a>
                <a href="<?php echo "http://localhost/Test/info-form.php?act=delete&mssv=" . $mssv ?>"
                   class="delete btn" onclick="confirmDelete(event)">Delete</a></td>
            </tr>;
        <?php } ?>
    </table>
<?php
include "footer.php";