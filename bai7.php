<?php
session_start();
$username = $_POST['username'] ?? null;
$_SESSION=[];
$password = $_POST['password'] ?? null;
$isAdmin = $username == 'admin' && $password == '12345';

?>

<?php include "header.php"; ?>

<?php
if (!$isAdmin) {
    ?>
    <div class="container">
        <form action="" method="post">
            <input type="text" name="username" placeholder="Username" required>
            <input type="password" name="password" placeholder="Password">
            <button type="submit">Submit</button>
        </form>
    </div>
    <?php
} else {
    ?>
    <h1>Hello Admin </h1>
    <?php
    header("Location: info-form.php");
}
?>

<?php include "footer.php" ?>