--a
CREATE DATABASE tutorial;

CREATE TABLE users(
id INT primary key auto_increment,
username VARCHAR(255),
email VARCHAR(255),
phone VARCHAR(15),
city_code VARCHAR(5)
);

CREATE TABLE products(
id INT primary key auto_increment,
name VARCHAR(255),
price INT,
in_stock INT,
description VARCHAR(255),
created_at DateTime
);

CREATE TABLE orders(
id INT primary key auto_increment,
code VARCHAR(255),
amount INT,
user_id INT,
adress VARCHAR(255),
created_at DateTime,
FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE order_items(
order_id INT,
product_id INT,
quantity INT,
PRIMARY KEY(order_id,product_id),
FOREIGN KEY (order_id) REFERENCES orders(id),
FOREIGN KEY (product_id) REFERENCES products(id)
);
--b
INSERT INTO users
VALUES (1,'tuandn','tuandn@kaiyouit.com','0123456789','HN'),
(2,'hungnh','hungnh@kaiyouit.com','0123456789','HA'),
(3,'longnh','longnh@kaiyouit.com','0123456789','QN'),
(4,'test','test@gmail.com','0123456789','HN')

INSERT INTO products
VALUES (1,'Ba Con Sâu',145000,11,'','2019-05-03 21:00:11'),
(2,'Bánh mì',15000,0,'Bánh mì gia truyền','2020-04-02 18:00:06'),
(3,'Cafe nâu đá L',45000,2,'Cafe nâu đá size L','2019-08-10 11:10:06'),
(4,'Sữa milo',6000,50,'','2019-01-01 09:20:00');

INSERT INTO orders
VALUES (1,'1601413565680',1504000,2,'Vinh - Nghệ An','2020-05-06 12:00:05'),
(2,'1601432105623',999000,3,'Hạ Long - Quảng Ninh','2020-09-30 8:00:00'),
(3,'1601433405611',110000,1,'Thạch Thất - Hà Nội','2020-08-01 16:50:04')

INSERT INTO order_items
VALUES (1,1,10),(2,1,5),(2,3,1),(3,1,1)

-- c
UPDATE users
SET phone = '0987654321'
WHERE city_code='HN';


-- d
DELETE FROM users WHERE username = 'test';

-- e
SELECT *
FROM orders
WHERE created_at > '2020-05-07';

--f
SELECT * FROM `orders` WHERE user_id = 1

-- g
SELECT name,in_stock
FROM `product`
WHERE in_stock <= 2

-- h

SELECT products.*, (SELECT SUM(order_items.quantity) from order_items WHERE order_items.product_id = id) AS total
FROM products
ORDER BY total DESC
LIMIT 2
