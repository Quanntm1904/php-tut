<?php
$errors = [
    'name' => '',
    'email' => '',
    'password' =>'',
    'verify' => '',
    'phone' => '',
    'adress' => '',
];


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // function for password verification
    function is_valid_passwords($password,$verify)
    {
        if ($password != $verify) {
            // error matching passwords
            return false;
        }
        // passwords match
        else return true;
    }

    $name = $_POST['name'] ?? null;
    if (empty($name)){
        $errors['name'] = "Name is required";
    } elseif (!preg_match("/.{2,30}/", $name)){
        $errors['name'] = "2-30 characters";
    }

    $email = $_POST['email'] ?? null;
    if (empty($email)){
        $errors['email'] = "Email is required";
    }elseif (!filter_var($email,FILTER_VALIDATE_EMAIL)){
        $errors['email'] = 'Invalid email format';
    }

    $password = $_POST['password'] ?? null;
    if (empty($password)){
        $errors['password'] = 'Password is required';
    }elseif (!preg_match("/(?=.*\d)(?=.*[A-Z]).{4,}/", $password)){
        $errors['password']="At least 1 number, 1 uppercase letter and 4 characters";
    }

    $verify = $_POST['verify'] ?? null;
    if (!is_valid_passwords($password, $verify)){
        $errors['verify'] = 'Please confirm your password';
    }

    $phone = $_POST['phone'] ?? null;
    if (empty($phone)){
        $errors['phone'] = 'Phone number is required';
    }elseif (!preg_match('/(?=.*\d){8,12}/', $phone)){
        $errors['phone'] = 'Phone number must include numbers and have 8-12 characters';
    }

    $adress = $_POST['adress'] ?? null;
    if (empty($adress)){
        $errors['adress'] = 'Adress is required';
    } elseif (!preg_match('/.{5,120}/', $adress)){
        $errors['adress'] = 'Adress have 5-120 characters';
    }
}
?>

<?php
include "header.php";
?>
<div class="signup">
    <h2>Đăng ký tài khoản</h2>
    <form action="" method="post">

        <label for="name">Tên</label> <span class="error">* <?php echo $errors['name'] ?></span>
        <input type="text" name="name" >

        <label for="email">Email</label> <span class="error">* <?php echo $errors['email']?></span>
        <input type="text" name="email" >

        <label for="password">Mật khẩu</label> <span class="error">* <?php echo $errors['password']?></span>
        <input type="password" name="password">

        <label for="verify">Xác nhận mật khẩu</label> <span class="error">* <?php echo $errors['verify']?></span>
        <input type="password" name="verify">

        <label for="phone">Số điện thoại</label> <span class="error">* <?php echo $errors['phone']?></span>
        <input type="text" name="phone">

        <label for="adress">Nhập địa chỉ</label> <span class="error">* <?php echo $errors['adress']?></span>
        <input type="text" name="adress">

        <input type="submit" value="Tạo tài khoản" id="submit">
    </form>
</div>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST"){
?>
<div class="info">
    <?php
    echo "<h2>Your Input:</h2>";
    echo $name;
    echo "<br>";
    echo $email;
    echo "<br>";
    echo $phone;
    echo "<br>";
    echo $adress;
    echo "<br>";
    ?>
</div>

<?php
}
include "footer.php";
?>
