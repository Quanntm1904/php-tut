<?php
$users = array(
    array(
        'name' => 'Shota Yamamoto',
        'site' => 'kaiyouit.com',
        'position' => 'founder'
    ),
    array(
        'name' => 'Nguyễn Huy Hùng',
        'site' => 'kaiyouit.com',
        'position' => 'founder'
    ),
    array(
        'name' => 'Nguyễn Hữu Long',
        'site' => 'kaiyouit.com',
        'position' => 'PM'
    ),
    array(
        'name' => 'Đỗ Như Tuấn',
        'site' => 'kaiyouit.com',
        'position' => 'leader'
    )
);
?>

<?php include "header.php" ?>
    <table>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>site</th>
            <th>Position</th>
        </tr>

        <?php
        foreach ($users as $i => $person) {
            echo '<tr>';
            echo '<td>' .($i + 1). '</td>';
            foreach ($person as $x => $value) {
                echo '<td>'. $value . '</td>';
            }
            echo '</tr>';
        } ?>
    </table>
<?php include "footer.php"; ?>