<?php
$district = array(
    array(
        "code" => "001",
        "type" => "quan",
        "name" => "Quận Ba Đình",
    ),
    array("code" => "002",
        "type" => "quan",
        "name" => "Quận Hoàn Kiếm",
    ),
    array(
        "code" => "003",
        "type" => "quan",
        "name" => "Quận Tây Hồ",
    ),
    array(
        "code" => "004",
        "type" => "quan",
        "name" => "Quận Long Biên",
    ),
    array(
        "code" => "005",
        "type" => "quan",
        "name" => "Quận Cầu Giấy",
    ),
    array(
        "code" => "006",
        "type" => "quan",
        "name" => "Quận Đống Đa",
    ),
    array(
        "code" => "007",
        "type" => "quan",
        "name" => "Quận Hai Bà Trưng",
    ),
    array(
        "code" => "008",
        "type" => "quan",
        "name" => "Quận Hoàng Mai",
    ),
    array(
        "code" => "009",
        "type" => "quan",
        "name" => "Quận Thanh Xuân",
    ),
    array(
        "code" => "016",
        "type" => "huyen",
        "name" => "Huyện Sóc Sơn",
    ),
    array(
        "code" => "017",
        "type" => "huyen",
        "name" => "Huyện Đông Anh",
    ),
    array(
        "code" => "018",
        "type" => "huyen",
        "name" => "Huyện Gia Lâm",
    )
);

include "header.php";
?>
<table>
    <tr>
        <th>Code</th>
        <th>Type</th>
        <th>Name</th>
    </tr>

    <?php
    foreach ($district as $local) {
        $class = $local['type'];
        echo '<tr class="'.$class.'">';

        foreach ($local as $key => $value) {
             if($value=='quan'){
                echo '<td class="quan">'. $value . '</td>';
            }
            elseif ($value == 'huyen'){
                echo '<td class="huyen">'. $value . '</td>';
            }
            echo '<td>'. $value . '</td>';
        }
        echo '</tr>';
    } ?>
</table>
<?php include "footer.php"; ?>












