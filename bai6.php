<?php

function equation($a, $b, $c)
{
    if ($a == 0 || $c == 0) {
        echo "1st and 3rd field cannot be 0 <br>";
    }
    if ($a != 0 && $c != 0) {
        $delta = $b * $b - 4 * $a * $c;
        if ($delta < 0) return false;
        elseif ($delta == 0) {
            return -$b / (2 * $a);
        } else {
            $solution['x1'] = (-$b + sqrt($delta)) / (2 * $a);
            $solution['x2'] = (-$b - sqrt($delta)) / (2 * $a);
            return $solution;
        }

    }
    return false;
}


$error = '';

?>

<?php
include "header.php"
?>
    <div class="calculate">
        <h1>Quadratic Equation Calculator</h1>
        <form action="" method="get">
            <input type="number" name="a"> <span>x <sup>2</sup> + </span> <input type="number" name="b">
            <span>x + </span>
            <input type="number" name="c">
            <input type="submit" value="Submit" id="submit">
        </form>

        <?php
        if (isset($_GET['a'], $_GET['b'], $_GET['c'])) {
            $a = $_GET['a'] ?? 0;
            $b = $_GET['b'] ?? 0;
            $c = $_GET['c'] ?? 0;
            $solution = equation($a, $b, $c);
            $type = gettype($solution);
            switch ($type) {
                case "boolean":
                    echo "No solution";
                    break;
                case "integer":
                    echo "Solution: " . $solution;
                    break;
                case "array":
                    echo $solution['x1'] . " ; " . $solution['x2'];
            }
        }

        ?>
    </div>

<?php
include "footer.php";
