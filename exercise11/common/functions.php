<?php
function isLogedin()
{
    return isset($_SESSION['users']);
}

function redirect($url)
{
    header("Location:$url");
}

function getError($key)
{
    $errors = $_SESSION['errors'] ?? [];
    $error = isset($errors[$key]) ? $errors[$key] : false;
    unset($_SESSION['errors'][$key]);
    return $error;
}

function getCurrentValue($key){
    $input = $_SESSION['input'];
    $currentValue = isset($input[$key]) ? $input[$key] : false;
    unset($_SESSION['input'][$key]);
    return $currentValue;
}