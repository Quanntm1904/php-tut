<?php

class StudentModel extends Database
{
    function getAll()
    {
        $res = mysqli_query($this->conn, 'SELECT * FROM students');
        if ($res) {
            return $this->fetch($res);
        }
        return [];
    }

    function insert($person)
    {
        extract($person);
        $res = mysqli_query(
            $this->conn,
            "INSERT INTO students VALUES ($mssv,$name, $birth,$phone,$adress)"
        );

        if ($res) {
            return $this->fetch();
        }
        return [];
    }
}