<?php

define('URL', str_replace('/index.php', '', $_SERVER['PHP_SELF']));
define('PATH', __DIR__);

session_start();
require 'config/constants.php';
require 'common/functions.php';
require 'models/Database.php';
require 'models/StudentModel.php';
require 'controllers/AuthController.php';
require 'controllers/HomeController.php';
require 'controllers/StudentController.php';

Database::instance();

//get action from GET method after url params is set
// default is 'home/index
$act = isset($_GET['act']) && !empty($_GET['act']) ? $_GET['act'] : 'home/index';

if (!isLogedin()){
    $act = 'auth/login';
}


$act = explode('/', $act);
$controller = ucfirst($act[0]) . 'Controller'; //get Controller
$method = $act[1];

try {
    /**
     * Dynamic instance class
     * It's same new ClassName()
     * Hoverer, if "ClassName" is value in a variable we need to implement like below
     */
    // create a ReflectionClass to get info of a class
    $ref = new ReflectionClass($controller);
    // create new instance of a class
    $instance = $ref->newInstance();

    // check method exists in controller class
    if (method_exists($controller, $method)) {
        // dynamic call method with method name is a variable
        $instance->$method();
    } else {
        // throw error if method not found in controller
        throw new Exception("Method $method not found in $controller");
    }

} catch (ReflectionException $e) {
    die("Controller $controller not found!");

} catch (Exception $e) {
    die($e->getMessage());
}

