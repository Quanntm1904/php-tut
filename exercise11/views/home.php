<?php
include "partials/header.php";
?>
<h2>Nhập thông tin cá nhân</h2>\
<form action="?act=student/add" method="POST">

    <span class="error">* <?php echo getError('mssv') ?></span>
    <input type="text" placeholder="MSSV" name="mssv" value="<?php echo getCurrentValue('mssv') ?>">
    <span class="error">* <?php echo getError('name') ?></span>
    <input type="text" placeholder="Ho va ten" name="name" value="<?php echo getCurrentValue('name') ?>">
    <span class="error">* <?php echo getError('birth') ?></span>
    <input type="date" placeholder="Nam sinh" name="birth" value="<?php echo getCurrentValue('birth') ?>">
    <span class="error">* <?php echo getError('phone') ?></span>
    <input type="text" placeholder="SDT" name="phone" value="<?php echo getCurrentValue('phone') ?>">
    <span class="error">* <?php echo getError('adress') ?></span>
    <input type="text" placeholder="Que quan" name="adress" value="<?php echo getCurrentValue('adress') ?>">

    <button type="submit" id="submit">Submit</button>

</form>

<table id="editable">
    <tr>
<!--        <th>STT</th>-->
        <th>MSSV</th>
        <th>Ho va ten</th>
        <th>Ngay sinh</th>
        <th>SDT</th>
        <th>Que Quan</th>

    </tr>

    <?php
    $i = 1;
    foreach ($students as $personal) {?>
        <tr>
            <td><?php echo $personal['mssv']?></td>
            <td><?php echo $personal['name']?></td>
            <td><?php echo $personal['birth']?></td>
            <td><?php echo $personal['phone']?></td>
            <td><?php echo $personal['address']?></td>
        </tr>
<!--        <td><a href="--><?php //echo "http://localhost/Test/info-form.php?act=edit&mssv=" . $mssv ?><!--" class="edit btn">Edit</a>-->
<!--            <a href="--><?php //echo "http://localhost/Test/info-form.php?act=delete&mssv=" . $mssv ?><!--"-->
<!--               class="delete btn" onclick="confirmDelete(event)">Delete</a></td>-->
<!--        </tr>;-->
    <?php } ?>
</table>
<?php
include 'partials/footer.php';
