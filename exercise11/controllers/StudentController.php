<?php

class StudentController
{

    private $model;

    function __construct()
    {
        $this->model = new StudentModel();
    }


    function add()
    {
        $person =[];
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $mssv = $_POST['mssv'] ?? null;
            if (empty($mssv)) {
                $errors['mssv'] = 'MSSV is required';
            } else {
                $person['mssv'] = $mssv;
            }

            $name = $_POST['name'] ?? null;
            if (empty($name)) {
                $errors['name'] = "Name is required";
            } elseif (!preg_match("/.{2,30}/", $name)) {
                $errors['name'] = "2-30 characters";
            } else {
                $person['name'] = $name;
            }

            $birth = $_POST['birth'] ?? null;
            if (empty($birth)) {
                $errors['birth'] = 'Birthday is required';
            } else {
                $person['birth'] = $birth;
            }

            $phone = $_POST['phone'] ?? null;
            if (empty($phone)) {
                $errors['phone'] = 'Phone number is required';
            } elseif (!preg_match('/(?=.*\d){8,12}/', $phone)) {
                $errors['phone'] = 'Phone number must include numbers and have 8-12 characters';
            } else {
                $person['phone'] = $phone;
            }

            $adress = $_POST['adress'] ?? null;
            if (empty($adress)) {
                $errors['adress'] = 'Adress is required';
            } elseif (!preg_match('/.{5,120}/', $adress)) {
                $errors['adress'] = 'Adress have 5-120 characters';
            } else {
                $person['adress'] = $adress;
            }

            if (empty($errors)) {
                $this->model->insert($person);
            } else{
                $_SESSION['errors'] = $errors;
                $_SESSION['input'] = $person;
            }
        }
        redirect(URL);
    }
}
