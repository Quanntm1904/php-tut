<?php
function isLogedin()
{
    return isset($_SESSION['current_user']);
}

function redirect($url)
{
    header('location:' . $url);
}

function getError($key)
{
    $errors = $_SESSION['errors'] ?? [];
    $error = isset($errors[$key]) ? $errors[$key] : false;
    unset($_SESSION['errors'][$key]);
    return $error;
}

function cleaSession($session)
{
    $_SESSION[$session] = [];
}
