<?php
function is_length_in($str, $min, $max) {
    $length = strlen($str);
    return $length >= $min && $length <= $max;
}
