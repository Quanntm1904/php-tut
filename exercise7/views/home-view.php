<?php
global $students;
require "partials/header.php";
?>

<div class="form__container">
    <div class="form__title">
        <h3>Add a new student</h3>
    </div>

    <div class="form__input">
        <form action="?act=student@add" method="post">
            <div class="form__group">
                <label for="code">Student code</label><span></span>
                <input type="text" name="code">
            </div>
            <div class="form__group">
                <label for="name">Full name</label> <span></span>
                <input type="text" name="name">
            </div>
            <div class="form__group">
                <label for="birth">Year of birth</label><span></span>
                <input type="number" name="birth">
            </div>
            <div class="form__group">
                <label for="phone">Phone number</label><span></span>
                <input type="text" name="phone">
            </div>

            <div class="form__group">
                <label for="adress">Adress</label><span></span>
                <input type="text" name="adress">
            </div>

            <button type="submit">Submit</button>
        </form>
    </div>
</div>

<div class="list">
    <div class="list__container">
        <table>
            <tr>
                <th>#</th>
                <th>Code</th>
                <th>Name</th>
                <th>Age</th>
                <th>Phone</th>
                <th>Adress</th>
            </tr>

            <?php
            foreach ($students as $index => $student){
            ?>
            <tr>
                <td><?php echo $index+1 ?></td>
                <td><?php echo $student['code'] ?></td>
                <td><?php echo $student['name']?></td>
                <td><?php echo $student['birth']?></td>
                <td><?php echo $student['phone']?></td>
                <td><?php echo $student['adress']?></td>
            </tr>
            <?php}?>
        </table>
    </div>
<?php
require "partials/footer.php";