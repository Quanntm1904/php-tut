<?php
require "partials/header.php";
?>

<div class="form__container">
    <div class="form__title">
        <h3>Login</h3>
    </div>
    <div class="form__input">
        <form action="?act=login" method="post">
            <div class="form__group">
                <label for="username">Username</label>
                <input type="text" name="username" placeholder="Username" required>    
            </div>
            <div class="form__group">
                <label for="password">Password</label>
                <input type="password" name="password" placeholder="Password">
            </div>
            <p class="error"></p>
            <button type="submit">Submit</button>
        </form>
    </div>
</div>

<?php
require "partials/footer.php";
