<?php
require PATH . '/common/validator.php';
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    redirect('');
    die();
}

$newStudent = [
    'code' => $_POST['code'] ?? '',
    'name' => $_POST['name'] ?? '',
    'birth' => $_POST['birth'] ?? '',
    'phone' => $_POST['phone'] ?? '',
    'adress' => $_POST['adress'] ?? ''
];

$errors = [];

if (!is_length_in($newStudent['code'], 5, 11)) {
    $errors['code'] = '* Code length have to be between 5 and 11';
}

if (!is_numeric($newStudent['phone'])) {
    $errors['phone'] = '* Invalid phone number field';
}

if (!is_numeric($newStudent['birth'])) {
    $errors['birth'] = '* Invalid Year of birth';
}

if (!is_length_in($newStudent['name'], 5, 50)) {
    $errors['name'] = '* Invalid name';
}

if (!is_length_in($newStudent['adress'], 5, 200)) {
    $errors = '* Invalid adress';
}

$students = $_SESSION['students'] ?? [];
foreach ($students as $index => $student) {
    if ($student['code'] == $newStudent['code']) {
        $errors['code'] = '* This student is already existed';
        break;
    }
}

if (empty($errors)){
$_SESSION['students'] = $newStudent;
}else{
    $_SESSION['errors'] = $errors;
    $_SESSION['input'] = $newStudent;
}

redirect(URL);