<?php

define('URL', str_replace('index.php', '', $_SERVER['PHP_SELF']));
define('PATH', __DIR__);

session_start();

if (!isLogedin()){
    require "controller/login.php";
    die();
}

$act = isset($_GET['act'])&&!empty($_GET['act']) ? $_GET['act'] :'home';

$act = str_replace('@', '/', $act);

require "controller/$act.php";

